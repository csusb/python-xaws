VERSION := $(shell cat VERSION)

sdist: dist/xaws-$(VERSION).tar.gz

dist/xaws-$(VERSION).tar.gz: pyproject.toml
	docker compose run --rm builder python -m build

upload: dist/xaws-$(VERSION).tar.gz
	docker compose run --rm builder python -m twine upload dist/xaws-$(VERSION).tar.gz dist/xaws-$(VERSION)-*.whl

clean:
	rm -rf dist
